#!/bin/bash

# Install a PHP version with the required extensions.
# This command could be divided into multiple ones, but this way we ensure that if
# one instruction fails, all fail.
curl -L -O https://github.com/phpbrew/phpbrew/raw/master/phpbrew \
    && chmod +x phpbrew \
    && mv phpbrew /usr/bin/phpbrew \
    && phpbrew init \
    && export PHPBREW_ROOT=/opt/phpbrew \
    && export PHPBREW_HOME=/opt/phpbrew \
    && source ~/.phpbrew/bashrc \
    && phpbrew install --mirror http://us.php.net -j 5 $PHP_VERSION \
        +default \
        +cgi \
        +curl \
        +exif \
        +ftp \
        +gettext \
        +gmp \
        +hash \
        +iconv \
        +intl \
        +mysql \
        +openssl \
        +session \
        +sqlite \
        +wddx \
        +xml \
        +xml_all \
        +zlib \
        -- --with-libdir=lib64 \
    && cp ~/.phpbrew/bashrc /opt/phpbrew/bashrc \
    && echo "export PHPBREW_ROOT=/opt/phpbrew" >> /etc/profile.d/phpbrew.sh \
    && echo "export PHPBREW_HOME=/opt/phpbrew" >> /etc/profile.d/phpbrew.sh \
    && echo "source /opt/phpbrew/bashrc" >> /etc/profile.d/phpbrew.sh \
    && chmod +x /etc/profile.d/phpbrew.sh \
    && phpbrew switch "$PHP_VERSION" \
    && sed -i 's/^;date\.timezone.*/date.timezone=UTC/' "$PHPBREW_HOME/php/$PHPBREW_PHP/etc/php.ini" \
    && sed -i 's/^memory_limit.*/memory_limit=1024M/' "$PHPBREW_HOME/php/$PHPBREW_PHP/etc/php.ini" \
    && phpbrew ext install igbinary  1.2.1  \
    && phpbrew ext install propro    1.0.2  \
    && phpbrew ext install raphf     1.1.2  \
    && phpbrew ext install pecl_http 2.5.5  \
    && phpbrew clean "$PHPBREW_PHP"