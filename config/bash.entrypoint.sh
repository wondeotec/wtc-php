#!/bin/bash

##############################
# Init                       #
##############################

# Load required files and set script modes.
set -m
source /etc/profile.d/phpbrew.sh

# Script variables.
PHP_INI=/config/php.ini

##############################
# Service configuration      #
##############################

# Copy new php.ini if any.
if [ -f "$PHP_INI" ]; then
    cp -f "$PHP_INI" "$PHPBREW_HOME/php/$PHPBREW_PHP/etc/php.ini"
fi

##############################
# User configuration         #
##############################

# Change user/group identifiers, as needed.
OLD_USER_ID=$(id -u $SERVICE_USER)
OLD_GROUP_ID=$(id -g $SERVICE_USER)

# Fetch the new user and group identifiers (default to the old ones).
NEW_USER_ID=${USER_ID:-$OLD_USER_ID}
NEW_GROUP_ID=${GROUP_ID:-$OLD_GROUP_ID}

# Change permissions if a new identifier is set.
if [ $OLD_USER_ID -ne $NEW_USER_ID ] || [ $OLD_GROUP_ID -ne $NEW_GROUP_ID ]; then
    usermod -u $NEW_USER_ID $SERVICE_USER
    groupmod -g $NEW_USER_GROUP $SERVICE_USER
    chown -Rhc --from=$OLD_USER_ID $NEW_USER_ID /
    chown -Rhc --from=:$OLD_GROUP_ID :$NEW_GROUP_ID /
fi >/dev/null 2>/dev/null

##############################
# Command configuration      #
##############################

# Execute original command, if any.
if [ "$#" -gt 0 ]; then cmd="$@"; su $SERVICE_USER -c "$cmd"; fi
