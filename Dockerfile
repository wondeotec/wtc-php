FROM centos:7

# Environment variables.
ENV PHP_VERSION   "<VERSION>"
ENV SERVICE_USER  "php"

# Add external dependencies.
COPY config /config

# Install system dependencies.
RUN chmod +x /config/*.sh \
    && curl -o /etc/yum.repos.d/isv:ownCloud:devel.repo http://download.opensuse.org/repositories/isv:/ownCloud:/devel/CentOS_7/isv:ownCloud:devel.repo \ 
    && yum install -y --nogpgcheck libcap-dummy \
    && yum install -y epel-release \
    && yum group install -y "Development Tools" \
    && buildDeps=" \
        libmcrypt-devel \
        libxslt-devel \
        libxml2-devel \
        bzip2-devel \
        gmp-devel \
        libicu-devel \
        libmcrypt-devel \
        readline-devel \
        libxslt-devel \
        openssl-devel \
        libcurl-devel \
        libmemcached-devel \
        gd-devel \
        libffi-devel \
    " \
    && yum install -y \
        php \
        $buildDeps \
    && /config/bash.install-php.sh \
    && yum groupremove -y "Development Tools" \
    && yum remove --setopt=tsflags=noscripts -y $buildDeps \
    && rm -rf /var/cache/yum/* \
    && yum clean all -y
    
# Configure users.
RUN groupadd $SERVICE_USER \
    && useradd -m -g $SERVICE_USER -s /bin/bash $SERVICE_USER \
    && passwd -d root \
    && passwd -d $SERVICE_USER \
    && chown $SERVICE_USER:$SERVICE_USER -R /var/lib/php/session

# Service definitions.
ENTRYPOINT ["/config/bash.entrypoint.sh"]
