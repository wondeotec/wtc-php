# WTC PHP

This repository provides a PHP runtime, configured with some sane defaults and extensions. The included PHP binaries and extensions are compiled using PHPBrew (see https://github.com/phpbrew/phpbrew for additional information).

[![](https://badge.imagelayers.io/wondeotec/wtc-php:latest.svg)](https://imagelayers.io/?images=wondeotec/wtc-php:latest 'Get your own badge on imagelayers.io')

## Running commands

The PHP command for this container is **php**. You can use it to run your own PHP scripts, for example:
```
#!bash
docker run -v /path/to/my/script.php:/script.php wondeotec/wtc-php php /script.php
```

Note that these scripts run as user **php**, so any changes to files outside the container may not map to your UID/GID. If you want to ensure that the container's user UID and GID matches your own, you can pass the following environment variables:
```
#!bash
docker run -v /path/to/my/script.php:/script.php wondeotec/wtc-php -e USER_ID=$UID -e GROUP_ID=$GID php /script.php
```

## Configuration

This image includes the default **php.ini** file distributed with PHP, with the following modifications:
* timezone is set to UTC;
* memory limit is set to 1024M;

You are encouraged to provide your own php.ini file. To do so, mount your configuration file in **/config/php.ini**, as such:

```
#!bash
docker run -v /path/to/my/php.ini:/config/php.ini wondeotec/wtc-php
```

## FPM variants

All provided versions have an FPM variant, which is essentially the same as the base version, but includes FPM and runs a FPM process pool by default (no command is necessary). FPM variants support all configuration options for the base version, including providing your own **php.ini** and changing the user/group id.

You are also able to provide your own **php-fpm.conf** file, as such:

```
#!bash
docker run -v /path/to/my/php-fpm.conf:/config/php-fpm.conf wondeotec/wtc-php
```

## Install extensions

Additional extensions can be installed using PHPBrew's build mechanism. An example, including the memcached extension:

```
#!docker
FROM wondeotec/wtc-php
RUN phpbrew ext install memcached
```

Please see [PHPBrew's official documentation](https://github.com/phpbrew/phpbrew) for additional information.